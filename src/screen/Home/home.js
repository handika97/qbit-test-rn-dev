import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  FlatList,
  Dimensions,
  Text,
  TouchableOpacity,
} from 'react-native';
import {Input, Button} from '../../component/cell';
import Product from '../../component/atoms/Product';
import Header from '../../component/atoms/header';
import {useDispatch} from 'react-redux';
import {benner, productShoes} from '../../utilities/json';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {ScrollView} from 'react-native-gesture-handler';

const windowWidth = Dimensions.get('window').width;

const Login = ({navigation}) => {
  const dispatch = useDispatch();
  const [activeSlide, setactiveSlide] = useState(0);
  const [HamburgerMenu, setHamburgerMenu] = useState(false);
  const [qty, setqty] = useState(0);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
      }}>
      <View
        style={{
          backgroundColor: '#f2f2f2',
          position: 'absolute',
          flex: 1,
          height: '100%',
          width: '100%',
          borderTopRightRadius: 220,
        }}></View>
      <ScrollView
        style={{
          flex: 1,
        }}>
        <Header
          qty={qty}
          HamburgerMenu={HamburgerMenu}
          onPress={() => setHamburgerMenu(!HamburgerMenu)}
        />
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 20,
            marginHorizontal: 13,
            marginVertical: 10,
          }}>
          Adidas App Store
        </Text>
        <View style={{marginTop: 40}}>
          <Carousel
            data={benner}
            renderItem={({item}) => (
              <View style={{maxHeight: 130}}>
                <Image
                  style={{
                    width: '100%',
                    height: '100%',
                    resizeMode: 'contain',
                  }}
                  source={item.banner_url}
                />
              </View>
            )}
            firstItem={0}
            autoplay={true}
            layout={'default'}
            layoutCardOffset={0}
            sliderWidth={windowWidth}
            itemWidth={windowWidth - 100}
            onSnapToItem={(index) => setactiveSlide(index)}
          />
        </View>
        <View style={{marginBottom: 20}}>
          <Pagination
            activeDotIndex={activeSlide}
            dotsLength={benner.length}
            dotStyle={{
              width: 10,
              height: 10,
              borderRadius: 5,
              backgroundColor: 'black',
            }}
          />
        </View>
        <View>
          <ScrollView horizontal={true} style={{height: 230}}>
            <FlatList
              data={productShoes}
              renderItem={({item}) => {
                return (
                  <Product
                    item={item}
                    qty={() => {
                      setqty(qty + 1);
                    }}
                  />
                );
              }}
              keyExtractor={(item) => item.id}
              horizontal={true}
            />
          </ScrollView>
        </View>
      </ScrollView>
    </View>
  );
};

export default Login;
