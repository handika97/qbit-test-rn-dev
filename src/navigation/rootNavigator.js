import React from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {Home} from '../screen/Home';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
const Tab = createMaterialBottomTabNavigator();

function AppNavigator() {
  return (
    <Tab.Navigator
      style={{backgroundColor: 'black'}}
      barStyle={{backgroundColor: '#eceded'}}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: ({focused}) => {},
          tabBarIcon: ({focused}) => (
            <FontAwesome5
              name="house-user"
              solid={focused ? true : false}
              color={focused ? '#464748' : 'white'}
              size={24}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Love"
        component={Home}
        options={{
          tabBarLabel: ({focused}) => {},
          tabBarIcon: ({focused}) => (
            <FontAwesome5
              name="heart"
              solid={focused ? true : false}
              color={'#464748'}
              size={24}
            />
          ),
        }}
      />
      <Tab.Screen
        name="User"
        component={Home}
        options={{
          tabBarLabel: ({focused}) => {},
          tabBarIcon: ({focused}) => (
            <FontAwesome5
              name="user"
              solid={focused ? true : false}
              color={'#464748'}
              size={24}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default AppNavigator;
