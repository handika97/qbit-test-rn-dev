import {createSlice} from '@reduxjs/toolkit';
// import Geolocation from 'react-native-geolocation-service';
import {useDispatch, useSelector} from 'react-redux';

const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    qty: 0,
  },
  reducers: {
    Add: (state, action) => {
      state.qty += 1;
    },
    Reset: (state) => {
      state.qty = 0;
    },
  },
});

export const {Add, Reset} = cartSlice.actions;

export default cartSlice.reducer;
