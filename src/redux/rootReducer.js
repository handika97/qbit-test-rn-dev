import cartReducer from './features/cart';
import {combineReducers} from '@reduxjs/toolkit';

const rootReducer = combineReducers({
  cart: cartReducer,
});

export default rootReducer;
