export const benner = [
  {
    banner_url: require('../assets/images/benner1.jpg'),
  },
  {
    banner_url: require('../assets/images/benner2.jpg'),
  },
  {
    banner_url: require('../assets/images/benner3.jpg'),
  },
  {
    banner_url: require('../assets/images/benner4.jpg'),
  },
];
export const productShoes = [
  {
    name: 'Adidas Ultra',
    url: require('../assets/images/product1.jpg'),
    id: 1,
  },
  {
    name: 'Adidas Swift',
    url: require('../assets/images/product2.jpg'),
    id: 2,
  },
  {
    name: 'Adidas Superstar',
    url: require('../assets/images/product3.jpg'),
    id: 3,
  },
  {
    name: 'Adidas 6',
    url: require('../assets/images/product4.jpg'),
    id: 4,
  },
];
