import React, {useState, useEffect} from 'react';
import {View, Text, Image, TouchableOpacity, TextInput} from 'react-native';

const Button = ({title, onPress = () => {}}) => {
  return (
    <TouchableOpacity
      style={{
        height: 40,
        backgroundColor: 'purple',
        top: 30,
        borderRadius: 10,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
      }}
      onPress={() => onPress()}>
      <Text style={{color: 'white', fontSize: 20}}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;
