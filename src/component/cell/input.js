import React, {useState, useEffect} from 'react';
import {View, Text, Image, TouchableOpacity, TextInput} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const Input = ({
  placeholder,
  password,
  value,
  onChange = () => {},
  icons,
  width,
  noBorder,
  type,
}) => {
  return (
    <View
      style={{
        borderBottomWidth: noBorder ? 0 : 1,
        borderColor: 'black',
        // width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
      }}>
      {icons && <FontAwesome5 name={icons} size={15} />}
      <TextInput
        placeholder={placeholder}
        secureTextEntry={password}
        value={value}
        keyboardType={type}
        onChangeText={(e) => onChange(e)}
        style={{
          flex: 1,
        }}
      />
    </View>
  );
};

export default Input;
