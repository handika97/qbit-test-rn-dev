import React from 'react';
import {View, Image, Text, TouchableOpacity} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ToastExample from '../../../Toast';

const List = ({item, qty = () => {}}) => {
  return (
    <View
      style={{
        height: 230,
        width: 150,
        borderRadius: 5,
        elevation: 5,
        marginHorizontal: 7,
      }}>
      <View
        style={{
          height: 190,
          width: 150,
        }}>
        <Image
          source={item.url}
          style={{
            height: '100%',
            width: '100%',
            resizeMode: 'cover',
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5,
          }}
        />
        <View
          style={{
            backgroundColor: '#747474',
            height: 40,
            borderBottomLeftRadius: 5,
            borderBottomRightRadius: 5,
            justifyContent: 'center',
            paddingHorizontal: 3,
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              padding: 2,
            }}>
            <TouchableOpacity
              style={{
                backgroundColor: 'white',
                borderRadius: 3,
                padding: 3,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                ToastExample.show(
                  `added ${item.name.toLowerCase()}`,
                  ToastExample.SHORT,
                );
                qty();
              }}>
              <FontAwesome5 name="plus" />
            </TouchableOpacity>
            <Text style={{color: 'white', fontSize: 13, marginLeft: 4}}>
              {item.name}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default List;
