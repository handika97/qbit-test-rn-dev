import React, {useEffect, useRef} from 'react';
import {View, Text, Animated} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
const Header = ({qty, HamburgerMenu, onPress = () => {}}) => {
  const opacity = useRef(new Animated.Value(0)).current;
  const TranslateX = useRef(new Animated.Value(10)).current;

  useEffect(() => {
    if (qty > 0) {
      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }).start();
      Animated.timing(TranslateX, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  }, [qty]);
  return (
    <View
      style={{
        padding: 17,
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}>
      <FontAwesome5
        name={HamburgerMenu ? 'times' : 'bars'}
        size={25}
        onPress={() => onPress()}
      />
      <Animated.View
        style={{
          minWidth: 45,
          padding: 4,
          transform: [{translateX: TranslateX}],
        }}>
        <FontAwesome5 name="shopping-cart" size={25} />

        <Animated.View
          style={{
            position: 'absolute',
            alignSelf: 'flex-end',
            backgroundColor: '#eb868e',
            padding: 2,
            borderRadius: 20,
            height: 20,
            width: 20,
            opacity: opacity,
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: 50,
          }}>
          <Text>{qty}</Text>
        </Animated.View>
      </Animated.View>
    </View>
  );
};

export default Header;
